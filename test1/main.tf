terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "ap-southeast-1"
  access_key = "AKIAQFRD4VWTEAB3NM2K"
  secret_key = "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"
}

# 1a. Create VPC
resource "aws_vpc" "stockbit-test-vpc" {
  cidr_block = "192.168.0.0/16"
  tags = {
    Name = "stockbit-test"
  }
}

# Creeate internet gateway
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.stockbit-test-vpc.id

  tags = {
    Name = "gw-test"
  }
}

# Create egress only
resource "aws_egress_only_internet_gateway" "stockbit-egress" {
  vpc_id = aws_vpc.stockbit-test-vpc.id
}

# Create route table
resource "aws_route_table" "stockbit-route-table" {
  vpc_id = aws_vpc.stockbit-test-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  route {
    ipv6_cidr_block        = "::/0"
    egress_only_gateway_id = aws_egress_only_internet_gateway.stockbit-egress.id
  }

  tags = {
    Name = "stockbit-rt"
  }
}


# 1b. Create subnet public
resource "aws_subnet" "stockbit-subnet-public" {
  vpc_id     = aws_vpc.stockbit-test-vpc.id
  cidr_block = "192.168.1.0/24"
  map_public_ip_on_launch = "true"
  availability_zone = "ap-southeast-1a"

  tags = {
    Name = "subnet-public"
  }
}

#1c. Create subnet private connected with 1 NAT Gateway
# Create subnet private
resource "aws_subnet" "stockbit-subnet-private" {
  vpc_id     = aws_vpc.stockbit-test-vpc.id
  cidr_block = "192.168.2.0/24"
  availability_zone = "ap-southeast-1a"

  tags = {
    Name = "subnet-private"
  }
}

# Create route table asosiation
resource "aws_route_table_association" "stockbit-as" {
  subnet_id      = aws_subnet.stockbit-subnet-private.id
  route_table_id = aws_route_table.stockbit-route-table.id
}

# Create NAT
resource "aws_nat_gateway" "stockbit-nat" {
  connectivity_type = "private"
  subnet_id         = aws_subnet.stockbit-subnet-private.id
}

# 1d. Create autoscaling group
# Create security group
resource "aws_security_group" "allow-web" {
  name        = "allow-web"
  description = "Allow WEB inbound traffic"
  vpc_id      = aws_vpc.stockbit-test-vpc.id

  ingress {
    description      = "default"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["192.168.0.0/16"]
  }

  ingress {
    description      = "https"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description      = "http"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description      = "ssh"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow-web"
  }
}

# Create network interface
resource "aws_network_interface" "stockbit-interface" {
  subnet_id       = aws_subnet.stockbit-subnet-private.id
  security_groups = [aws_security_group.allow-web.id]

}

# Create EIP
resource "aws_eip" "stockbit-eip" {
  vpc                       = true
  network_interface         = aws_network_interface.stockbit-interface.id
  depends_on		    = [aws_internet_gateway.gw]
}

# Create autoscale

data "aws_availability_zones" "available" {}

resource "aws_launch_configuration" "config" {
  name_prefix     = "launch_config"
  image_id        = "ami-055147723b7bca09a"
  instance_type   = "t2.medium"
  key_name        = "main-key"
  associate_public_ip_address = true
  security_groups = [aws_security_group.allow-web.id]
  
  # Install docker, docker-compose, and aws-cli on server
  user_data = <<-EOF
              #!/bin/bash
              sudo apt-get update -y
              curl -fsSL https://get.docker.com -o get-docker.sh
              sudo sh get-docker.sh
              usermod -aG docker ubuntu
              systemctl enable docker
              systemctl start docker
              sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
              sudo chmod +x /usr/local/bin/docker-compose
              sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
              curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
              unzip awscliv2.zip
              sudo ./aws/install
              EOF
}

# Create autoscaling group
resource "aws_autoscaling_group" "stockbit-autoscale" {
  name_prefix = "stockbit-autoscale"

  launch_configuration = aws_launch_configuration.config.name
  vpc_zone_identifier  = [aws_subnet.stockbit-subnet-private.id]

  min_size = 2
  max_size = 5

  tags = [
    {
      key                 = "web-application"
      value               = "stockbit-webapp"
      propagate_at_launch = true
    },
  ]
}


# Create autoscaling plan
resource "aws_autoscalingplans_scaling_plan" "stockbit-autoscale-plan" {
  name = "stockbit-autoscale-plans"

  application_source {
    tag_filter {
      key    = "web-application"
      values = ["stockbit-webapp"]
    }
  }

  scaling_instruction {
    max_capacity       = 5
    min_capacity       = 2
    resource_id        = format("autoScalingGroup/%s", aws_autoscaling_group.stockbit-autoscale.name)
    scalable_dimension = "autoscaling:autoScalingGroup:DesiredCapacity"
    service_namespace  = "autoscaling"

    target_tracking_configuration {
      predefined_scaling_metric_specification {
        predefined_scaling_metric_type = "ASGAverageCPUUtilization"
      }

      target_value = 45
    }
  }
}
