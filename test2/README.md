HOW TO USE
- Install docker, docker-compose, aws-cli on server
- login to aws using command: aws config
- login aws ecr using command : docker login -u AWS -p <password> <aws_account_id>.dkr.ecr.<region>.amazonaws.com
- running gitlab ci